-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 26, 2020 at 10:11 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boilerplate`
--

-- --------------------------------------------------------

--
-- Table structure for table `assetindexdata`
--

CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `uploaderId` int(11) DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assettransformindex`
--

CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `error` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assettransforms`
--

CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups`
--

CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups_sites`
--

CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `changedattributes`
--

CREATE TABLE `changedattributes` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `changedfields`
--

CREATE TABLE `changedfields` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, NULL, '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'a29231a4-ab03-468b-b0de-673b59d7b974');

-- --------------------------------------------------------

--
-- Table structure for table `craftidtokens`
--

CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deprecationerrors`
--

CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) UNSIGNED DEFAULT NULL,
  `message` text,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `drafts`
--

CREATE TABLE `drafts` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `trackChanges` tinyint(1) NOT NULL DEFAULT '0',
  `dateLastMerged` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elementindexsettings`
--

CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

CREATE TABLE `elements` (
  `id` int(11) NOT NULL,
  `draftId` int(11) DEFAULT NULL,
  `revisionId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `draftId`, `revisionId`, `fieldLayoutId`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, NULL, NULL, 'craft\\elements\\User', 1, 0, '2020-08-26 09:48:30', '2020-08-26 09:48:30', NULL, '94c9702a-56e7-4f26-88cd-7e5a26c507de');

-- --------------------------------------------------------

--
-- Table structure for table `elements_sites`
--

CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements_sites`
--

INSERT INTO `elements_sites` (`id`, `elementId`, `siteId`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, NULL, NULL, 1, '2020-08-26 09:48:30', '2020-08-26 09:48:30', '0a292dd6-228a-4817-a8be-c9d8e1f2e1d6');

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entrytypes`
--

CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleTranslationMethod` varchar(255) NOT NULL DEFAULT 'site',
  `titleTranslationKeyFormat` text,
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fieldgroups`
--

CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldgroups`
--

INSERT INTO `fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Common', '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'd63fa9a0-c02c-4386-84e0-75ca6a6daba0');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayoutfields`
--

CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouts`
--

CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouttabs`
--

CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `elements` text,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `globalsets`
--

CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gqlschemas`
--

CREATE TABLE `gqlschemas` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `scope` text,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gqltokens`
--

CREATE TABLE `gqltokens` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `expiryDate` datetime DEFAULT NULL,
  `lastUsed` datetime DEFAULT NULL,
  `schemaId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `configVersion` char(12) NOT NULL DEFAULT '000000000000',
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `version`, `schemaVersion`, `maintenance`, `configVersion`, `fieldVersion`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, '3.5.6', '3.5.13', 0, 'wjvodnlwhlqh', 'sjojjhfhelui', '2020-08-26 09:48:30', '2020-08-26 09:48:31', 'be385e77-981f-485c-b50f-a54f1765c23b');

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocks`
--

CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocktypes`
--

CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `track` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `track`, `name`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'craft', 'Install', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '1ed0d508-ee09-4eef-8c99-8bb674111f4f'),
(2, 'craft', 'm150403_183908_migrations_table_changes', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '74b87175-8d48-4844-9bb6-f568c3df520c'),
(3, 'craft', 'm150403_184247_plugins_table_changes', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '6d42bece-508a-4fe3-b276-de18f56c0888'),
(4, 'craft', 'm150403_184533_field_version', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'cab427d6-36a0-4920-891d-6ed388411e77'),
(5, 'craft', 'm150403_184729_type_columns', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'c85608c7-7028-44ee-aa41-6ac6505632f9'),
(6, 'craft', 'm150403_185142_volumes', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'b0db161e-d8ff-4b9d-bf9d-432a7c47ee7e'),
(7, 'craft', 'm150428_231346_userpreferences', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '04e0b21c-1f58-43c8-93a7-9493f6bf5f8c'),
(8, 'craft', 'm150519_150900_fieldversion_conversion', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '32317099-74cc-4876-a355-6b47059632a2'),
(9, 'craft', 'm150617_213829_update_email_settings', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '401f1a29-01c2-4939-9de7-ac08a886dd60'),
(10, 'craft', 'm150721_124739_templatecachequeries', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '8849c7f0-024e-4a32-9ccc-2bcdb1e93c2b'),
(11, 'craft', 'm150724_140822_adjust_quality_settings', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'c12bff15-0035-4ef5-9fbc-c55a22d5c497'),
(12, 'craft', 'm150815_133521_last_login_attempt_ip', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '873a1422-0f3c-443c-8f81-adf8d708bd7b'),
(13, 'craft', 'm151002_095935_volume_cache_settings', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '88be9dae-72bc-41e3-bfef-b862415a48bc'),
(14, 'craft', 'm151005_142750_volume_s3_storage_settings', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '0c072806-b10a-443e-ad28-fd7495c6eb2b'),
(15, 'craft', 'm151016_133600_delete_asset_thumbnails', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '1b78afe8-b153-4ac6-bbc5-83f72c015471'),
(16, 'craft', 'm151209_000000_move_logo', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'acce9d02-3377-4535-b6b0-b1ab25a68753'),
(17, 'craft', 'm151211_000000_rename_fileId_to_assetId', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '1f3631d5-71ad-4e8c-bf89-7046950de6b8'),
(18, 'craft', 'm151215_000000_rename_asset_permissions', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '45ba034e-73ec-4c29-9c80-0f9e0fc4a20e'),
(19, 'craft', 'm160707_000001_rename_richtext_assetsource_setting', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2811f547-0432-4359-a401-fe172618862b'),
(20, 'craft', 'm160708_185142_volume_hasUrls_setting', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '66725b2c-9319-472a-bc59-2a9b731a7238'),
(21, 'craft', 'm160714_000000_increase_max_asset_filesize', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '127c60be-d420-40c3-84a9-386e2a08890e'),
(22, 'craft', 'm160727_194637_column_cleanup', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', '9bc6dd65-cbd4-4504-a171-1a926bbf3d07'),
(23, 'craft', 'm160804_110002_userphotos_to_assets', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '4c5f6ebe-33fd-42d7-9c9c-20cbb6f6d95f'),
(24, 'craft', 'm160807_144858_sites', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '10f71a06-7e81-4149-868e-cf64db7ad76a'),
(25, 'craft', 'm160829_000000_pending_user_content_cleanup', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '24c90477-9fa6-42f8-98eb-aa296194c79a'),
(26, 'craft', 'm160830_000000_asset_index_uri_increase', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '4270a934-0889-4a2d-b842-356314effc51'),
(27, 'craft', 'm160912_230520_require_entry_type_id', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '6537e47b-0a4e-4301-9bed-ece7dd33e670'),
(28, 'craft', 'm160913_134730_require_matrix_block_type_id', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'f71b1191-9b8f-404e-9520-17a01b15b4ab'),
(29, 'craft', 'm160920_174553_matrixblocks_owner_site_id_nullable', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2c4c82f9-7983-4d51-a35b-94420d945506'),
(30, 'craft', 'm160920_231045_usergroup_handle_title_unique', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'cf9952d6-7aaa-4a8b-9c0d-a4d7e0f5a2aa'),
(31, 'craft', 'm160925_113941_route_uri_parts', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '936ba7c5-22f1-447f-8e78-81c968bb7087'),
(32, 'craft', 'm161006_205918_schemaVersion_not_null', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '7d0faf69-944a-4d5b-a341-eb2d86f286d4'),
(33, 'craft', 'm161007_130653_update_email_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '3cfde162-ef9f-43f3-82e4-04187ce3495e'),
(34, 'craft', 'm161013_175052_newParentId', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '272c2934-ee6e-41ad-9d51-f09ff7634428'),
(35, 'craft', 'm161021_102916_fix_recent_entries_widgets', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'f64719aa-ab25-4da2-b35b-8070abf1ebe2'),
(36, 'craft', 'm161021_182140_rename_get_help_widget', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '10f13730-f9e4-4433-b2ff-865ec2f03219'),
(37, 'craft', 'm161025_000000_fix_char_columns', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'ae7beb0e-e49f-4ccb-814a-4aeb0fa90511'),
(38, 'craft', 'm161029_124145_email_message_languages', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'bdf55d03-674c-4501-a0da-6fe6137cd4d4'),
(39, 'craft', 'm161108_000000_new_version_format', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '9ae2af42-30d7-4b4e-b52e-6d56b06241d5'),
(40, 'craft', 'm161109_000000_index_shuffle', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'c822504d-aa91-4cf8-89b8-3b70118548e1'),
(41, 'craft', 'm161122_185500_no_craft_app', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '21dc3775-9753-4da8-b3fb-c8235060ab9e'),
(42, 'craft', 'm161125_150752_clear_urlmanager_cache', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e4a555f7-8199-4962-9b3a-e396b69d420a'),
(43, 'craft', 'm161220_000000_volumes_hasurl_notnull', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e9924ce7-ade3-4adf-ab9f-ee18b48ef3b9'),
(44, 'craft', 'm170114_161144_udates_permission', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0febc7ea-6a0f-4ad3-90cd-fd4a109dd43c'),
(45, 'craft', 'm170120_000000_schema_cleanup', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '12aacf0e-a6dd-4678-a1b9-548d9bd0a0de'),
(46, 'craft', 'm170126_000000_assets_focal_point', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'c26fe2a5-c518-431e-b4c1-1fa3f65d8c81'),
(47, 'craft', 'm170206_142126_system_name', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'b6f07c62-cc03-4fcc-8e3f-ad198158c914'),
(48, 'craft', 'm170217_044740_category_branch_limits', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '335801e2-9e57-42e7-bd67-8cfb480f5820'),
(49, 'craft', 'm170217_120224_asset_indexing_columns', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'ff5492e7-0ce8-4444-8791-c3220880042b'),
(50, 'craft', 'm170223_224012_plain_text_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e1c1cd17-f1e0-44f3-9687-b852433dbf93'),
(51, 'craft', 'm170227_120814_focal_point_percentage', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '64f2e4a1-554b-4888-b16c-96d7d007b602'),
(52, 'craft', 'm170228_171113_system_messages', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '4a1ab2ea-4372-4a2c-9222-e30901853f5b'),
(53, 'craft', 'm170303_140500_asset_field_source_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '5b524e00-986a-4fe1-b15e-2ec373671129'),
(54, 'craft', 'm170306_150500_asset_temporary_uploads', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'ed7806cd-cc92-4356-b77a-a1e220e9a6ef'),
(55, 'craft', 'm170523_190652_element_field_layout_ids', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'd390bda4-918f-4a7d-b525-195e4cac86a1'),
(56, 'craft', 'm170612_000000_route_index_shuffle', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '49afaa25-83d4-493f-bb13-5fb082e1157e'),
(57, 'craft', 'm170621_195237_format_plugin_handles', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '4f3869fe-d14d-46d0-bca8-f75c5aab4722'),
(58, 'craft', 'm170630_161027_deprecation_line_nullable', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '49d1a4d3-a81e-4937-b807-7b6f7efc4aa7'),
(59, 'craft', 'm170630_161028_deprecation_changes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0d65d6ee-deaa-4b12-a6e3-99c7ce63a263'),
(60, 'craft', 'm170703_181539_plugins_table_tweaks', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e2f69d02-9ab4-44cb-85b9-52d8b64c92b3'),
(61, 'craft', 'm170704_134916_sites_tables', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'd7b4fd59-3326-4995-8cae-7e5867b83e7e'),
(62, 'craft', 'm170706_183216_rename_sequences', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '03ea3891-7524-4929-90ab-be7db59521fd'),
(63, 'craft', 'm170707_094758_delete_compiled_traits', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '1045a032-c57a-4d98-9d89-632c1a29b192'),
(64, 'craft', 'm170731_190138_drop_asset_packagist', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '724948b6-d35c-45ff-ae50-99635339eb3b'),
(65, 'craft', 'm170810_201318_create_queue_table', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '1a31d4d1-814c-4d75-b654-e2e52223b432'),
(66, 'craft', 'm170903_192801_longblob_for_queue_jobs', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'f381d87a-b53d-4c83-9706-ff98506a0397'),
(67, 'craft', 'm170914_204621_asset_cache_shuffle', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '9be8fc22-ba38-4999-b86a-3ebb7f9f0dea'),
(68, 'craft', 'm171011_214115_site_groups', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'fe3654a0-ab00-420a-bee4-20b4c7933e25'),
(69, 'craft', 'm171012_151440_primary_site', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '4a178966-3c6e-49fd-8b58-c50f3f372e67'),
(70, 'craft', 'm171013_142500_transform_interlace', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'd5c7d8d1-feca-40ff-8264-f3f62e318b19'),
(71, 'craft', 'm171016_092553_drop_position_select', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '62fb8eea-4328-405f-9463-f5a1e22250e9'),
(72, 'craft', 'm171016_221244_less_strict_translation_method', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'b17ae8f3-244d-4228-92ef-0f442f0a7f53'),
(73, 'craft', 'm171107_000000_assign_group_permissions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '73d9ac14-9cda-411b-a6ab-7fc76d2d3fd4'),
(74, 'craft', 'm171117_000001_templatecache_index_tune', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'fca7c543-6ba0-48d3-92cc-b8df24621dea'),
(75, 'craft', 'm171126_105927_disabled_plugins', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '94d13a7f-094d-49f2-bb66-5eee9042df1e'),
(76, 'craft', 'm171130_214407_craftidtokens_table', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '9a5a01f9-c85b-476f-91c6-ba96b8ba0173'),
(77, 'craft', 'm171202_004225_update_email_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'dd1bf99e-a13f-44a9-af67-27f419cb1b7f'),
(78, 'craft', 'm171204_000001_templatecache_index_tune_deux', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '5c942a4f-48b1-492a-a5a7-05ba605760bb'),
(79, 'craft', 'm171205_130908_remove_craftidtokens_refreshtoken_column', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e83b879c-6243-4455-84b7-442297d7ae17'),
(80, 'craft', 'm171218_143135_longtext_query_column', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '656c9eea-cff5-4e20-9e97-4727fe38dc47'),
(81, 'craft', 'm171231_055546_environment_variables_to_aliases', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '23430088-b8ae-4d6e-9b69-668890ae8e67'),
(82, 'craft', 'm180113_153740_drop_users_archived_column', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'dca8efa5-042c-4eb2-ac8c-a5a4f7d26f65'),
(83, 'craft', 'm180122_213433_propagate_entries_setting', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'fc6e5d08-f1ac-4308-8b15-5b92728cde11'),
(84, 'craft', 'm180124_230459_fix_propagate_entries_values', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '7f9443b1-103d-4b0e-95fb-46029256edd1'),
(85, 'craft', 'm180128_235202_set_tag_slugs', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'bc205587-a4e9-4a71-ba0d-fa8e17052fc7'),
(86, 'craft', 'm180202_185551_fix_focal_points', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '78d7c453-7b4a-4566-b0ef-7bdda6e42f1d'),
(87, 'craft', 'm180217_172123_tiny_ints', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '90807643-67f8-4865-a2e2-264c7c12f234'),
(88, 'craft', 'm180321_233505_small_ints', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '1188c11d-d08b-4c78-90ca-ae832ecc14a6'),
(89, 'craft', 'm180328_115523_new_license_key_statuses', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2ef1b554-06a0-4c94-9fca-829dffec824e'),
(90, 'craft', 'm180404_182320_edition_changes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '700dc0e7-da58-4255-a3a4-d041748f0e75'),
(91, 'craft', 'm180411_102218_fix_db_routes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '4a7f853c-6fab-4479-a16f-dad54959583a'),
(92, 'craft', 'm180416_205628_resourcepaths_table', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '8b945159-5bec-4d1c-bf51-33c24d80132e'),
(93, 'craft', 'm180418_205713_widget_cleanup', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'ca08cf89-c2c3-45fa-bc8d-cf303cb0bc1c'),
(94, 'craft', 'm180425_203349_searchable_fields', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '13258e27-57a4-4aac-a639-e31d6623ffa8'),
(95, 'craft', 'm180516_153000_uids_in_field_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '040e5c6a-f343-4680-8476-b88ce6a5ed9f'),
(96, 'craft', 'm180517_173000_user_photo_volume_to_uid', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '1256ccd8-8e61-4293-9630-99d75955a16b'),
(97, 'craft', 'm180518_173000_permissions_to_uid', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'acf69ad2-429c-4c69-80c2-3b7e2db009a6'),
(98, 'craft', 'm180520_173000_matrix_context_to_uids', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'ecc6fa56-3053-42b6-a670-20e8c6ca2914'),
(99, 'craft', 'm180521_172900_project_config_table', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'efb3e466-f8cc-4c2d-9518-29c0d43736d9'),
(100, 'craft', 'm180521_173000_initial_yml_and_snapshot', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2eba2750-c71e-4199-9a67-4bcd878104f1'),
(101, 'craft', 'm180731_162030_soft_delete_sites', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '79a74f5a-3f73-491d-bcb2-eeee7bbb57d9'),
(102, 'craft', 'm180810_214427_soft_delete_field_layouts', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '85ae5ed5-bf19-4ab8-aec7-ff22f8d43e03'),
(103, 'craft', 'm180810_214439_soft_delete_elements', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '531b2cf4-8bf6-46e3-9763-faf900b6587c'),
(104, 'craft', 'm180824_193422_case_sensitivity_fixes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '62866586-b3ae-45a6-90c0-588ea54c2672'),
(105, 'craft', 'm180901_151639_fix_matrixcontent_tables', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'fe490058-1c01-4164-a71c-e43000af18a2'),
(106, 'craft', 'm180904_112109_permission_changes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '99d52863-9d8a-467c-b05b-50a14cfdc8a1'),
(107, 'craft', 'm180910_142030_soft_delete_sitegroups', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '94f9fedf-b0fc-4782-ad17-b0ce88993c69'),
(108, 'craft', 'm181011_160000_soft_delete_asset_support', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '646dbed3-9bdc-4bc5-8e61-331ffa858ad3'),
(109, 'craft', 'm181016_183648_set_default_user_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'bbe2e85b-535c-468d-ab87-67bb3d8c304c'),
(110, 'craft', 'm181017_225222_system_config_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'c4fb90fd-9dda-4591-84c5-81deee2da70e'),
(111, 'craft', 'm181018_222343_drop_userpermissions_from_config', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e4975c42-642f-43ec-817d-2bf8b634320e'),
(112, 'craft', 'm181029_130000_add_transforms_routes_to_config', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '259f6939-30fc-4c30-8858-c2216b1dc60f'),
(113, 'craft', 'm181112_203955_sequences_table', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0cb38ec6-d09d-4251-9118-f147a28c3904'),
(114, 'craft', 'm181121_001712_cleanup_field_configs', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e3548ff2-dad8-4185-ac21-5309c2953e74'),
(115, 'craft', 'm181128_193942_fix_project_config', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'a4ef0967-8d8e-44ca-a3e2-01f0e1dad52c'),
(116, 'craft', 'm181130_143040_fix_schema_version', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e6d8c033-0d30-43c7-b02a-f8739dc575f2'),
(117, 'craft', 'm181211_143040_fix_entry_type_uids', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'c6c09973-37f5-481b-91be-b4c3316af80b'),
(118, 'craft', 'm181217_153000_fix_structure_uids', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '77717d0d-8741-4402-ae59-38950f3ce4a3'),
(119, 'craft', 'm190104_152725_store_licensed_plugin_editions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '76672d10-456f-4fe9-8c9a-56be5da92496'),
(120, 'craft', 'm190108_110000_cleanup_project_config', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '1ee83ecf-895a-4fa2-89e9-163476213c67'),
(121, 'craft', 'm190108_113000_asset_field_setting_change', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '56e6e3b1-4dc4-42ad-9c87-f3cb7f188ed4'),
(122, 'craft', 'm190109_172845_fix_colspan', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '388750d1-9805-4b9f-963c-2df22214fce5'),
(123, 'craft', 'm190110_150000_prune_nonexisting_sites', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'c124b8ab-654c-4f86-abff-b46430e0f8a6'),
(124, 'craft', 'm190110_214819_soft_delete_volumes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'fcd04895-5aee-452a-8b48-4b10e46dbe41'),
(125, 'craft', 'm190112_124737_fix_user_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'dc0b711f-44f0-45b2-9540-29ccdcf714a5'),
(126, 'craft', 'm190112_131225_fix_field_layouts', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0ae031bc-1f79-47a6-98c4-c20148f71da5'),
(127, 'craft', 'm190112_201010_more_soft_deletes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'adcdec2e-dd47-4332-8b38-57f391c5de7e'),
(128, 'craft', 'm190114_143000_more_asset_field_setting_changes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0e9067e7-d40a-464b-8e6f-9a585a67fe99'),
(129, 'craft', 'm190121_120000_rich_text_config_setting', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '56cd3761-aa6a-4fb3-882b-41da050961c8'),
(130, 'craft', 'm190125_191628_fix_email_transport_password', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '94928eee-6cb7-4b88-bbf0-d15502c44a17'),
(131, 'craft', 'm190128_181422_cleanup_volume_folders', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'a1903987-df02-4edf-a8e9-b8d4958d057f'),
(132, 'craft', 'm190205_140000_fix_asset_soft_delete_index', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'a8802ed2-f0df-404c-81ca-f3e7b0c38270'),
(133, 'craft', 'm190218_143000_element_index_settings_uid', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2cde3a65-7246-4945-b92b-85362db5eb72'),
(134, 'craft', 'm190312_152740_element_revisions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '8a8e4155-a910-4717-a211-0e9d4717d494'),
(135, 'craft', 'm190327_235137_propagation_method', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'a3623cb7-9e78-483c-af17-0340d74694b3'),
(136, 'craft', 'm190401_223843_drop_old_indexes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '3e83c388-307d-439e-ad9d-c94da3102ec2'),
(137, 'craft', 'm190416_014525_drop_unique_global_indexes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '304e1239-3d04-4af4-9fc9-34eeb1896e44'),
(138, 'craft', 'm190417_085010_add_image_editor_permissions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'b9b51abc-6344-4be7-b604-ba9a618f74d8'),
(139, 'craft', 'm190502_122019_store_default_user_group_uid', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '36b407ec-f572-4c67-a60a-185a228fdae1'),
(140, 'craft', 'm190504_150349_preview_targets', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e5c274d8-8a75-4d1a-847e-8d73b29a248c'),
(141, 'craft', 'm190516_184711_job_progress_label', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'a88b81f0-8f1f-4a05-99cd-0d4b7883b1d6'),
(142, 'craft', 'm190523_190303_optional_revision_creators', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0b5c83a0-7b40-4ec6-9842-2fa227829b48'),
(143, 'craft', 'm190529_204501_fix_duplicate_uids', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '199ddd52-74af-4ceb-8183-64bedb0d3f83'),
(144, 'craft', 'm190605_223807_unsaved_drafts', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0d5d9ad7-4de9-47a8-8416-9907af3ec79f'),
(145, 'craft', 'm190607_230042_entry_revision_error_tables', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2aec1ae5-1c96-40af-b404-2ba0c84e0d65'),
(146, 'craft', 'm190608_033429_drop_elements_uid_idx', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e6befb19-f448-4efd-9b50-275589048c65'),
(147, 'craft', 'm190617_164400_add_gqlschemas_table', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '1a798e0f-a4b4-4b4f-baee-b5720c6e6bf7'),
(148, 'craft', 'm190624_234204_matrix_propagation_method', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0df96cda-636e-4fef-a65f-c375846c9942'),
(149, 'craft', 'm190711_153020_drop_snapshots', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '80767dfe-d9fc-407f-b242-46d811406b15'),
(150, 'craft', 'm190712_195914_no_draft_revisions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'ba5986da-5f06-413b-80d5-ba462244ae36'),
(151, 'craft', 'm190723_140314_fix_preview_targets_column', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '37dbbb9a-11ae-41e6-ac4d-0f5798b70347'),
(152, 'craft', 'm190820_003519_flush_compiled_templates', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'fda761a0-3672-4363-90d0-64aecea8a026'),
(153, 'craft', 'm190823_020339_optional_draft_creators', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e16fc238-bd7f-48ca-b519-0123ad280f90'),
(154, 'craft', 'm190913_152146_update_preview_targets', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'c62fedc0-5957-45b4-98f9-86e852aee6dc'),
(155, 'craft', 'm191107_122000_add_gql_project_config_support', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '259218cf-3e03-45e3-a565-e0ddff1f2bf6'),
(156, 'craft', 'm191204_085100_pack_savable_component_settings', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '47bcbb1e-f00c-494b-bcd9-76509070c537'),
(157, 'craft', 'm191206_001148_change_tracking', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '42d0d17f-6c39-4c08-8555-df2acc5d1f87'),
(158, 'craft', 'm191216_191635_asset_upload_tracking', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2ce728de-21dd-45da-9bfc-3743010e1203'),
(159, 'craft', 'm191222_002848_peer_asset_permissions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '00e79287-ff0a-4182-9cec-a8c1af1d81aa'),
(160, 'craft', 'm200127_172522_queue_channels', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '933f8a35-c663-40c6-aadf-64cf4f817c3d'),
(161, 'craft', 'm200211_175048_truncate_element_query_cache', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2e518cc2-9253-402f-a1e5-0d0d5f485a66'),
(162, 'craft', 'm200213_172522_new_elements_index', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '726543a6-7a49-4e93-a608-2bae19281c9d'),
(163, 'craft', 'm200228_195211_long_deprecation_messages', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '0565ce28-4584-40b0-b633-e15d24f4459b'),
(164, 'craft', 'm200306_054652_disabled_sites', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'b0995a77-9a8a-4791-b3d9-edbd75423cd8'),
(165, 'craft', 'm200522_191453_clear_template_caches', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'd295d879-9ec2-4a7b-aa09-e7e9edf5ab11'),
(166, 'craft', 'm200606_231117_migration_tracks', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '634429b5-06c3-4bd2-ab91-da66262c49cf'),
(167, 'craft', 'm200619_215137_title_translation_method', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2d524db5-3cbb-4f76-8af0-129737c5152c'),
(168, 'craft', 'm200620_005028_user_group_descriptions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '75560ca1-594d-43a0-ba46-71656aaae255'),
(169, 'craft', 'm200620_230205_field_layout_changes', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '66c89c76-1cba-4413-859b-279506c9d7a5'),
(170, 'craft', 'm200625_131100_move_entrytypes_to_top_project_config', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e9382b65-44cb-4413-b054-d08966a57a6a'),
(171, 'craft', 'm200629_112700_remove_project_config_legacy_files', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2981d57d-1914-44c8-b9fc-a348a1f36735'),
(172, 'craft', 'm200630_183000_drop_configmap', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'c8fb47bd-d16c-494e-a587-86f315f9deb5'),
(173, 'craft', 'm200715_113400_transform_index_error_flag', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'e380feeb-57d6-4650-86f1-65ef36c68e90'),
(174, 'craft', 'm200716_110900_replace_file_asset_permissions', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'a3a0555b-184d-490e-8645-a063a42f186f'),
(175, 'craft', 'm200716_153800_public_token_settings_in_project_config', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '6b613b1e-100b-4f5f-a94e-f98429787637'),
(176, 'craft', 'm200720_175543_drop_unique_constraints', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '7bd25e1a-1750-40ba-b25e-4b797f017f63'),
(177, 'craft', 'm200825_051217_project_config_version', '2020-08-26 09:48:31', '2020-08-26 09:48:31', '2020-08-26 09:48:31', 'bb2fd219-8f31-48f1-b488-667439fd64db');

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projectconfig`
--

CREATE TABLE `projectconfig` (
  `path` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projectconfig`
--

INSERT INTO `projectconfig` (`path`, `value`) VALUES
('dateModified', '1598435310'),
('email.fromEmail', '\"keelin@emagine.ie\"'),
('email.fromName', '\"boilerplate\"'),
('email.transportType', '\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"'),
('fieldGroups.d63fa9a0-c02c-4386-84e0-75ca6a6daba0.name', '\"Common\"'),
('siteGroups.7c279cc7-41e8-402d-93d6-5e14da91418b.name', '\"boilerplate\"'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.baseUrl', '\"$PRIMARY_SITE_URL\"'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.handle', '\"default\"'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.hasUrls', 'true'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.language', '\"en-US\"'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.name', '\"boilerplate\"'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.primary', 'true'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.siteGroup', '\"7c279cc7-41e8-402d-93d6-5e14da91418b\"'),
('sites.b6ece5f9-7f59-4643-a3df-e44f73312294.sortOrder', '1'),
('system.edition', '\"solo\"'),
('system.live', 'true'),
('system.name', '\"boilerplate\"'),
('system.schemaVersion', '\"3.5.13\"'),
('system.timeZone', '\"America/Los_Angeles\"'),
('users.allowPublicRegistration', 'false'),
('users.defaultGroup', 'null'),
('users.photoSubpath', 'null'),
('users.photoVolumeUid', 'null'),
('users.requireEmailVerification', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL DEFAULT 'queue',
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) UNSIGNED NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resourcepaths`
--

CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revisions`
--

CREATE TABLE `revisions` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `searchindex`
--

CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `searchindex`
--

INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`) VALUES
(1, 'username', 0, 1, ' admin '),
(1, 'firstname', 0, 1, ''),
(1, 'lastname', 0, 1, ''),
(1, 'fullname', 0, 1, ''),
(1, 'email', 0, 1, ' keelin emagine ie '),
(1, 'slug', 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) NOT NULL DEFAULT 'all',
  `previewTargets` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sections_sites`
--

CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sequences`
--

CREATE TABLE `sequences` (
  `name` varchar(255) NOT NULL,
  `next` int(11) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shunnedmessages`
--

CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sitegroups`
--

CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sitegroups`
--

INSERT INTO `sitegroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'boilerplate', '2020-08-26 09:48:30', '2020-08-26 09:48:30', NULL, '7c279cc7-41e8-402d-93d6-5e14da91418b');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `groupId`, `primary`, `enabled`, `name`, `handle`, `language`, `hasUrls`, `baseUrl`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 1, 'boilerplate', 'default', 'en-US', 1, '$PRIMARY_SITE_URL', 1, '2020-08-26 09:48:30', '2020-08-26 09:48:30', NULL, 'b6ece5f9-7f59-4643-a3df-e44f73312294');

-- --------------------------------------------------------

--
-- Table structure for table `structureelements`
--

CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) UNSIGNED DEFAULT NULL,
  `lft` int(11) UNSIGNED NOT NULL,
  `rgt` int(11) UNSIGNED NOT NULL,
  `level` smallint(6) UNSIGNED NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

CREATE TABLE `structures` (
  `id` int(11) NOT NULL,
  `maxLevels` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `systemmessages`
--

CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `taggroups`
--

CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecacheelements`
--

CREATE TABLE `templatecacheelements` (
  `id` int(11) NOT NULL,
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecachequeries`
--

CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecaches`
--

CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) UNSIGNED DEFAULT NULL,
  `usageCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `description` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups_users`
--

CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions`
--

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_usergroups`
--

CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_users`
--

CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpreferences`
--

CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL,
  `preferences` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userpreferences`
--

INSERT INTO `userpreferences` (`userId`, `preferences`) VALUES
(1, '{\"language\":\"en-US\"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `photoId`, `firstName`, `lastName`, `email`, `password`, `admin`, `locked`, `suspended`, `pending`, `lastLoginDate`, `lastLoginAttemptIp`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `hasDashboard`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'admin', NULL, NULL, NULL, 'keelin@emagine.ie', '$2y$13$gWEt41rTk0cEp3GbprC1q./AUDN3egDLe/G9tvkwTQftZmfmcsG46', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, '2020-08-26 09:48:30', '2020-08-26 09:48:30', '2020-08-26 09:48:30', 'ced971e0-c681-4312-a3a8-c1fee3602899');

-- --------------------------------------------------------

--
-- Table structure for table `volumefolders`
--

CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `volumes`
--

CREATE TABLE `volumes` (
  `id` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  ADD KEY `assetindexdata_volumeId_idx` (`volumeId`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assets_filename_folderId_idx` (`filename`,`folderId`),
  ADD KEY `assets_folderId_idx` (`folderId`),
  ADD KEY `assets_volumeId_idx` (`volumeId`),
  ADD KEY `assets_uploaderId_fk` (`uploaderId`);

--
-- Indexes for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`);

--
-- Indexes for table `assettransforms`
--
ALTER TABLE `assettransforms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assettransforms_name_idx` (`name`),
  ADD KEY `assettransforms_handle_idx` (`handle`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_groupId_idx` (`groupId`),
  ADD KEY `categories_parentId_fk` (`parentId`);

--
-- Indexes for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorygroups_name_idx` (`name`),
  ADD KEY `categorygroups_handle_idx` (`handle`),
  ADD KEY `categorygroups_structureId_idx` (`structureId`),
  ADD KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `categorygroups_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  ADD KEY `categorygroups_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `changedattributes`
--
ALTER TABLE `changedattributes`
  ADD PRIMARY KEY (`elementId`,`siteId`,`attribute`),
  ADD KEY `changedattributes_elementId_siteId_dateUpdated_idx` (`elementId`,`siteId`,`dateUpdated`),
  ADD KEY `changedattributes_siteId_fk` (`siteId`),
  ADD KEY `changedattributes_userId_fk` (`userId`);

--
-- Indexes for table `changedfields`
--
ALTER TABLE `changedfields`
  ADD PRIMARY KEY (`elementId`,`siteId`,`fieldId`),
  ADD KEY `changedfields_elementId_siteId_dateUpdated_idx` (`elementId`,`siteId`,`dateUpdated`),
  ADD KEY `changedfields_siteId_fk` (`siteId`),
  ADD KEY `changedfields_fieldId_fk` (`fieldId`),
  ADD KEY `changedfields_userId_fk` (`userId`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `content_siteId_idx` (`siteId`),
  ADD KEY `content_title_idx` (`title`);

--
-- Indexes for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craftidtokens_userId_fk` (`userId`);

--
-- Indexes for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`);

--
-- Indexes for table `drafts`
--
ALTER TABLE `drafts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drafts_creatorId_fk` (`creatorId`),
  ADD KEY `drafts_sourceId_fk` (`sourceId`);

--
-- Indexes for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`);

--
-- Indexes for table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elements_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `elements_type_idx` (`type`),
  ADD KEY `elements_enabled_idx` (`enabled`),
  ADD KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  ADD KEY `elements_archived_dateDeleted_draftId_revisionId_idx` (`archived`,`dateDeleted`,`draftId`,`revisionId`),
  ADD KEY `elements_draftId_fk` (`draftId`),
  ADD KEY `elements_revisionId_fk` (`revisionId`);

--
-- Indexes for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `elements_sites_siteId_idx` (`siteId`),
  ADD KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  ADD KEY `elements_sites_enabled_idx` (`enabled`),
  ADD KEY `elements_sites_uri_siteId_idx` (`uri`,`siteId`);

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entries_postDate_idx` (`postDate`),
  ADD KEY `entries_expiryDate_idx` (`expiryDate`),
  ADD KEY `entries_authorId_idx` (`authorId`),
  ADD KEY `entries_sectionId_idx` (`sectionId`),
  ADD KEY `entries_typeId_idx` (`typeId`),
  ADD KEY `entries_parentId_fk` (`parentId`);

--
-- Indexes for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entrytypes_name_sectionId_idx` (`name`,`sectionId`),
  ADD KEY `entrytypes_handle_sectionId_idx` (`handle`,`sectionId`),
  ADD KEY `entrytypes_sectionId_idx` (`sectionId`),
  ADD KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `entrytypes_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldgroups_name_idx` (`name`);

--
-- Indexes for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  ADD KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  ADD KEY `fieldlayoutfields_fieldId_idx` (`fieldId`);

--
-- Indexes for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouts_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `fieldlayouts_type_idx` (`type`);

--
-- Indexes for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayouttabs_layoutId_idx` (`layoutId`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fields_handle_context_idx` (`handle`,`context`),
  ADD KEY `fields_groupId_idx` (`groupId`),
  ADD KEY `fields_context_idx` (`context`);

--
-- Indexes for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `globalsets_name_idx` (`name`),
  ADD KEY `globalsets_handle_idx` (`handle`),
  ADD KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `gqlschemas`
--
ALTER TABLE `gqlschemas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gqltokens`
--
ALTER TABLE `gqltokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `gqltokens_accessToken_unq_idx` (`accessToken`),
  ADD UNIQUE KEY `gqltokens_name_unq_idx` (`name`),
  ADD KEY `gqltokens_schemaId_fk` (`schemaId`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matrixblocks_ownerId_idx` (`ownerId`),
  ADD KEY `matrixblocks_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocks_typeId_idx` (`typeId`),
  ADD KEY `matrixblocks_sortOrder_idx` (`sortOrder`);

--
-- Indexes for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matrixblocktypes_name_fieldId_idx` (`name`,`fieldId`),
  ADD KEY `matrixblocktypes_handle_fieldId_idx` (`handle`,`fieldId`),
  ADD KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `migrations_track_name_unq_idx` (`track`,`name`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plugins_handle_unq_idx` (`handle`);

--
-- Indexes for table `projectconfig`
--
ALTER TABLE `projectconfig`
  ADD PRIMARY KEY (`path`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queue_channel_fail_timeUpdated_timePushed_idx` (`channel`,`fail`,`timeUpdated`,`timePushed`),
  ADD KEY `queue_channel_fail_timeUpdated_delay_idx` (`channel`,`fail`,`timeUpdated`,`delay`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  ADD KEY `relations_sourceId_idx` (`sourceId`),
  ADD KEY `relations_targetId_idx` (`targetId`),
  ADD KEY `relations_sourceSiteId_idx` (`sourceSiteId`);

--
-- Indexes for table `resourcepaths`
--
ALTER TABLE `resourcepaths`
  ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `revisions`
--
ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `revisions_sourceId_num_unq_idx` (`sourceId`,`num`),
  ADD KEY `revisions_creatorId_fk` (`creatorId`);

--
-- Indexes for table `searchindex`
--
ALTER TABLE `searchindex`
  ADD PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`);
ALTER TABLE `searchindex` ADD FULLTEXT KEY `searchindex_keywords_idx` (`keywords`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sections_handle_idx` (`handle`),
  ADD KEY `sections_name_idx` (`name`),
  ADD KEY `sections_structureId_idx` (`structureId`),
  ADD KEY `sections_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  ADD KEY `sections_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `sequences`
--
ALTER TABLE `sequences`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_uid_idx` (`uid`),
  ADD KEY `sessions_token_idx` (`token`),
  ADD KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  ADD KEY `sessions_userId_idx` (`userId`);

--
-- Indexes for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`);

--
-- Indexes for table `sitegroups`
--
ALTER TABLE `sitegroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sitegroups_name_idx` (`name`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sites_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `sites_handle_idx` (`handle`),
  ADD KEY `sites_sortOrder_idx` (`sortOrder`),
  ADD KEY `sites_groupId_fk` (`groupId`);

--
-- Indexes for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  ADD KEY `structureelements_root_idx` (`root`),
  ADD KEY `structureelements_lft_idx` (`lft`),
  ADD KEY `structureelements_rgt_idx` (`rgt`),
  ADD KEY `structureelements_level_idx` (`level`),
  ADD KEY `structureelements_elementId_idx` (`elementId`);

--
-- Indexes for table `structures`
--
ALTER TABLE `structures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `structures_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `systemmessages`
--
ALTER TABLE `systemmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  ADD KEY `systemmessages_language_idx` (`language`);

--
-- Indexes for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taggroups_name_idx` (`name`),
  ADD KEY `taggroups_handle_idx` (`handle`),
  ADD KEY `taggroups_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_groupId_idx` (`groupId`);

--
-- Indexes for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  ADD KEY `templatecacheelements_elementId_idx` (`elementId`);

--
-- Indexes for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  ADD KEY `templatecachequeries_type_idx` (`type`);

--
-- Indexes for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  ADD KEY `templatecaches_siteId_idx` (`siteId`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tokens_token_unq_idx` (`token`),
  ADD KEY `tokens_expiryDate_idx` (`expiryDate`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usergroups_handle_idx` (`handle`),
  ADD KEY `usergroups_name_idx` (`name`);

--
-- Indexes for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  ADD KEY `usergroups_users_userId_idx` (`userId`);

--
-- Indexes for table `userpermissions`
--
ALTER TABLE `userpermissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_name_unq_idx` (`name`);

--
-- Indexes for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  ADD KEY `userpermissions_usergroups_groupId_idx` (`groupId`);

--
-- Indexes for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  ADD KEY `userpermissions_users_userId_idx` (`userId`);

--
-- Indexes for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_uid_idx` (`uid`),
  ADD KEY `users_verificationCode_idx` (`verificationCode`),
  ADD KEY `users_email_idx` (`email`),
  ADD KEY `users_username_idx` (`username`),
  ADD KEY `users_photoId_fk` (`photoId`);

--
-- Indexes for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  ADD KEY `volumefolders_parentId_idx` (`parentId`),
  ADD KEY `volumefolders_volumeId_idx` (`volumeId`);

--
-- Indexes for table `volumes`
--
ALTER TABLE `volumes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volumes_name_idx` (`name`),
  ADD KEY `volumes_handle_idx` (`handle`),
  ADD KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `volumes_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widgets_userId_idx` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assettransforms`
--
ALTER TABLE `assettransforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categorygroups`
--
ALTER TABLE `categorygroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drafts`
--
ALTER TABLE `drafts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `elements`
--
ALTER TABLE `elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `elements_sites`
--
ALTER TABLE `elements_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `entrytypes`
--
ALTER TABLE `entrytypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `globalsets`
--
ALTER TABLE `globalsets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gqlschemas`
--
ALTER TABLE `gqlschemas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gqltokens`
--
ALTER TABLE `gqltokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revisions`
--
ALTER TABLE `revisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections_sites`
--
ALTER TABLE `sections_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sitegroups`
--
ALTER TABLE `sitegroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `structureelements`
--
ALTER TABLE `structureelements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `structures`
--
ALTER TABLE `structures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `systemmessages`
--
ALTER TABLE `systemmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `taggroups`
--
ALTER TABLE `taggroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templatecaches`
--
ALTER TABLE `templatecaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpermissions`
--
ALTER TABLE `userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userpreferences`
--
ALTER TABLE `userpreferences`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `volumefolders`
--
ALTER TABLE `volumefolders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `volumes`
--
ALTER TABLE `volumes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_uploaderId_fk` FOREIGN KEY (`uploaderId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `changedattributes`
--
ALTER TABLE `changedattributes`
  ADD CONSTRAINT `changedattributes_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedattributes_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedattributes_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `changedfields`
--
ALTER TABLE `changedfields`
  ADD CONSTRAINT `changedfields_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedfields_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changedfields_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `drafts`
--
ALTER TABLE `drafts`
  ADD CONSTRAINT `drafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `drafts_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `elements_draftId_fk` FOREIGN KEY (`draftId`) REFERENCES `drafts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `elements_revisionId_fk` FOREIGN KEY (`revisionId`) REFERENCES `revisions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `entries`
--
ALTER TABLE `entries`
  ADD CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `entries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gqltokens`
--
ALTER TABLE `gqltokens`
  ADD CONSTRAINT `gqltokens_schemaId_fk` FOREIGN KEY (`schemaId`) REFERENCES `gqlschemas` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `relations`
--
ALTER TABLE `relations`
  ADD CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `revisions`
--
ALTER TABLE `revisions`
  ADD CONSTRAINT `revisions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `revisions_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `volumes`
--
ALTER TABLE `volumes`
  ADD CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `widgets`
--
ALTER TABLE `widgets`
  ADD CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

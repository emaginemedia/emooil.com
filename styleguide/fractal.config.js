'use strict';

/* Create a new Fractal instance and export it for use elsewhere if required */
const fractal = module.exports = require('@frctl/fractal').create();

/* Set the title of the project */
fractal.set('project.title', 'Emagine Boilerplate');

// register the engine adapter for your components
// Default (twig)
// const engineAdapter = require('@frctl/twig')({
//   paths: ['src/components'],
//
//   // if pristine is set to true, bundled filters, functions, tests
//   // and tags are not registered.
//   // default is false
//   pristine: false,
//
//   // if importContext is set to true, all include calls are passed
//   // the component's context
//   // default is false
//   // importContext: false,
//
//   // use custom handle prefix
//   // this will change your includes to {% include '%button' %}
//   // default is '@'
//   // handlePrefix: '%',
//
//   // set a base path for twigjs
//   // Setting base to '/' will make sure all resolved render paths
//   // start at the defined components dir, instead of being relative.
//   // default is null
//   // base: '/',
//
//   // register custom filters
//   filters: {
//     // usage: {{ label|capitalize }}
//     capitalize: function(str) {
//       if (!str) return '';
//
//       return str.charAt(0).toUpperCase() + str.slice(1);
//     }
//   },
//
//   // register custom functions
//   functions: {
//     // usage: {{ capitalize(label) }}
//     capitalize: function(str) {
//       if (!str) return '';
//
//       return str.charAt(0).toUpperCase() + str.slice(1);
//     }
//   },
//
//   // register custom tests
//   tests: {
//     // usage: {% if label is equalToNull %}
//     equalToNull: function(param) {
//       return param === null;
//     }
//   },
//
//   // register custom tags
//   tags: {
//     // flag: function(Twig) {
//     //   // usage: {% flag "ajax" %}
//     //   // all credit to https://github.com/twigjs/twig.js/wiki/Extending-twig.js-With-Custom-Tags
//     //   return {
//     //     // unique name for tag type
//     //     type: "flag",
//     //     // regex match for tag (flag white-space anything)
//     //     regex: /^flag\s+(.+)$/,
//     //     // this is a standalone tag and doesn't require a following tag
//     //     next: [ ],
//     //     open: true,
//     //
//     //     // runs on matched tokens when the template is loaded. (once per template)
//     //     compile: function (token) {
//     //       var expression = token.match[1];
//     //
//     //       // Compile the expression. (turns the string into tokens)
//     //       token.stack = Twig.expression.compile.apply(this, [{
//     //         type:  Twig.expression.type.expression,
//     //         value: expression
//     //       }]).stack;
//     //
//     //       delete token.match;
//     //       return token;
//     //     },
//     //
//     //     // Runs when the template is rendered
//     //     parse: function (token, context, chain) {
//     //       // parse the tokens into a value with the render context
//     //       var name = Twig.expression.parse.apply(this, [token.stack, context]),
//     //         output = '';
//     //
//     //       flags[name] = true;
//     //
//     //       return {
//     //         chain: false,
//     //         output: output
//     //       };
//     //     }
//     //   };
//     // }
//   }
// });
// Uncomment this if you wish to use nunjucks instead of twig
const engineAdapter = require('@frctl/nunjucks')({
  paths: ['src/components'],
  env: {
    // Nunjucks environment opts: https://mozilla.github.io/nunjucks/api.html#configure
  },
  filters: {
    // filter-name: function filterFunc(){}
  },
  globals: {
    // global-name: global-val
  },
  extensions: {
    // extension-name: function extensionFunc(){}
  }
});

fractal.components.set('default.preview', '@preview');

// const mandelbrot = require('@frctl/mandelbrot'); // require the Mandelbrot theme module
// https://fractal.build/guide/customisation/web-themes.html#template-customisation
// https://fractal.build/guide/web/default-theme.html#configuration
// https://github.com/frctl/fractal/issues/214
const mandelbrot = require('@frctl/mandelbrot');

const emagineModifiedMandelbrot = mandelbrot({
    // favicon: __dirname + './../public_html/assets/icons/favicon.ico',
    // favicon: __dirname + '/assets/favicon.ico',
    favicon: '/assets/favicon.ico',
    // favicon: __dirname + '/assets/icons/favicon.ico',
    // skin: 'white',
    "skin": "orange",
    nav: ['docs', 'components'],
    // styles: [
    //     'default',
    //     // '/css/mytheme.css'
    // ]
});

// emagineModifiedMandelbrot.addLoadPath(__dirname + '/theme');
fractal.web.theme(emagineModifiedMandelbrot);
//
// emagineModifiedMandelbrot.addStatic(__dirname + '/theme');

fractal.components.engine(engineAdapter); // register the Nunjucks adapter for your components

// Default (Twig file ext)
fractal.components.set('ext', '.twig'); // look for files with a .twig file extension
// Be sure to change your nunjucksAdapter if you wish to use nunjucks instead of twig
// fractal.components.set('ext', '.nunj'); // look for files with a .nunj file extension

/* Tell Fractal to use BrowserSync to watch the filesystem and reload the web UI when changes occur. */
fractal.web.set('server.sync', true);

/* Pass BrowserSync options to fractal */
// fractal.web.set('server.syncOptions', {
//     open: true,
//     browser: ['google chrome', 'firefox'],
//     notify: true
// });

/* Tell Fractal which preview to default to */
fractal.components.set('default.preview', '@preview');

/* Tell Fractal where the components will live */
fractal.components.set('path', __dirname + '/src/components');

/* Tell Fractal where the documentation pages will live */
fractal.docs.set('path', __dirname + '/src/docs');

/* Tell Fractal where we are compiling our styleguide specific assets */
fractal.web.set('static.path', __dirname + './../public_html/assets');

/* Set the static HTML build destination */
// fractal.web.set('builder.dest', __dirname + '/build');
fractal.web.set('builder.dest', __dirname + './../public_html/styleguide');

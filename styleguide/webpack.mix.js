let mix = require('laravel-mix');
// const Jarvis = require('webpack-jarvis');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const {GenerateSW} = require('workbox-webpack-plugin');
// const {InjectManifest} = require('workbox-webpack-plugin');

/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for your application, as well as bundling up your JS files.
|
*/

mix.setPublicPath(path.normalize('../public_html/assets/'))
	// .setResourceRoot(path.normalize('./'))
	.js('src/js/main.js', 'js/')
	.sass('src/scss/main.scss', 'css/')
	// .autoload({})
  // Effectively makes it easier to trace or debug issues
	.sourceMaps()

	// Customise Webpack Config Settings
	.webpackConfig({
		plugins: [
      new GenerateSW({
        swDest: './../sw.js',
        clientsClaim: true,
        skipWaiting: true,
        importsDirectory: './../assets',
        runtimeCaching: [{
          // Match any same-origin request that contains 'api'.
          // urlPattern: /api/,
          // Change Url Pattern to reflect your live site (remember that SWs will only work on HTTPS)
          urlPattern: new RegExp('^https://emagine\.ie/'),
          // Apply a network-first strategy.
          handler: 'NetworkFirst',
          options: {
            // Fall back to the cache after 10 seconds.
            networkTimeoutSeconds: 10,
            // Use a custom cache name for this route.
            cacheName: 'emagine-api-cache',
            // Configure custom cache expiration.
            expiration: {
              maxEntries: 5,
              maxAgeSeconds: 60,
            },
            // Configure background sync.
            backgroundSync: {
              name: 'emagine-queue',
              options: {
                maxRetentionTime: 60 * 60,
              },
            },
            // Configure which responses are considered cacheable.
            cacheableResponse: {
              statuses: [0, 200],
              headers: {'x-test': 'true'},
            },
            // Configure the broadcast update plugin.
            // broadcastUpdate: {
            //   channelName: 'emagine-update-channel',
            // },
            // Add in any additional plugin logic you need.
            plugins: [
              // {cacheDidUpdate: () => /* custom plugin code */}
            ],
            // matchOptions and fetchOptions are used to configure the handler.
            fetchOptions: {
              mode: 'no-cors',
            },
            matchOptions: {
              ignoreSearch: true,
            },
          },
        }, {
          // To match cross-origin requests, use a RegExp that matches
          // the start of the origin:
          urlPattern: new RegExp('^https://emagine\.ie/'),
          handler: 'StaleWhileRevalidate',
          options: {
            cacheableResponse: {
              statuses: [0, 200]
            }
          }
        }]
      }),
      // new InjectManifest({option: 'value'}),
			// new Jarvis({
			// 	port: 1337 // optional: set a port
			// }),
			// new BundleAnalyzerPlugin()
		],
		resolve: {
			// Resolve Jquery so we can import things like Slick
			alias: {
        '@': path.resolve('../public_html/assets/'),
				jquery: "jquery/src/jquery"
				// You may need to resolve additional npm packages like isotope
				// isotope: 'isotope-layout'
			}
		},
		module: {
			rules: [
				{
					test: /\.scss/,
					enforce: "pre",
					loader: 'import-glob-loader'
				},
        {
         test: /\.jsx?$/,
         enforce: "post",
         exclude: /node_modules(?!\/foundation-sites)|bower_components/,
         // include: [
         //   path.resolve(__dirname, 'src'),
         //   path.resolve(__dirname, 'node_modules/foundation-sites')
         //  ],
         use: [{
           loader: 'babel-loader',
           options: mix.config.babel()
           // options: {
           //   presets: ['env']
           // }
         }]
        }
			]
		}
	})

	// Webpack / Mix Options
	.options({
    // Set Process CSS Urls to true if you don't want mix to rewrite relative css paths
		// Basically mix treats these as being relative to uncompiled css.
		processCssUrls: false,
		// postCss: [
		// 	require('autoprefixer')({
		// 		browsers: ['last 2 versions', 'ie 9-11'],
		// 		cascade: false
		// 	})
		// ],
    // switches from uglify to terser for js api configs for webpack 4
    // https://github.com/terser-js/terser
    terser: {
        terserOptions: {
            warnings: true
        }
    }
	})

	// Extract Vendor Libs
	// You should add any libs you're using in package.json, this will add a request, but reduce cache busting on vendor libraries
	// https://github.com/JeffreyWay/laravel-mix/blob/master/docs/extract.md
	.extract([
		'foundation-sites',
    'jquery',
    'lazysizes',
		'object-fit-images',
	    // 'slick-carousel',
	    // 'magnific-popup',
	    // 'infinite-scroll',
	    // 'isotope-layout',
	    // 'vanilla-fitvids'
	]);

// Copy image & fonts files to public folder, as we can't have multiple resource roots without too much messing
// This is only needed if you allow processing of css urls
// mix.copyDirectory('assets/images', '../public_html/assets/images');
// mix.copyDirectory('assets/fonts', '../public_html/assets/fonts');

// Copy Files to public_html to be used by craft or EE in a different location,or to use in wordpress, change paths to reflect the directory structure
//mix.copyDirectory('../public_html/assets/css', '../public_html/wp-content/themes/theme-name/assets/css');;
//mix.copyDirectory('../public_html/assets/images', '../public_html/wp-content/themes/theme-name/assets/images');
//mix.copyDirectory('../public_html/assets/js', '../public_html/wp-content/themes/theme-name/assets/js');
//mix.copyDirectory('../public_html/assets/vendor', '../public_html/wp-content/themes/theme-name/assets/vendor');

// Version your assets when in [prod] (Optional)
// if (mix.inProduction()) {
// 	mix.version();
// }

// Full API as of 5.0.5
// https://github.com/JeffreyWay/laravel-mix
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.preact(src, output); <-- Identical to mix.js(), but registers Preact compilation.
// mix.coffee(src, output); <-- Identical to mix.js(), but registers CoffeeScript compilation.
// mix.ts(src, output); <-- TypeScript support. Requires tsconfig.json to exist in the same folder as webpack.mix.js
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.test');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.babelConfig({}); <-- Merge extra Babel configuration (plugins, etc.) with Mix's default.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.when(condition, function (mix) {}) <-- Call function if condition is true.
// mix.override(function (webpackConfig) {}) <-- Will be triggered once the webpack config object has been fully generated by Mix.
// mix.dump(); <-- Dump the generated webpack config object to the console.
// mix.extend(name, handler) <-- Extend Mix's API with your own components.
// mix.options({
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   terser: {}, // Terser-specific options. https://github.com/webpack-contrib/terser-webpack-plugin#options
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });

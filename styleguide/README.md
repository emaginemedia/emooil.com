# Boilerplate v3.0 - Styleguide

This is the styleguide portion of Boilerplate v3.0

This includes:

- [Laravel Mix](https://github.com/JeffreyWay/laravel-mix)
- [Twig](http://twig.sensiolabs.org/) (well, nunjucks for the moment, fairly similar though)
- [Fractal](http://fractal.build/)

## Setup

- Run `npm install`, or `yarn` to install dependencies from `package.json`
- Run `npm run start` to kick off the styleguide, and webpack to manage your assets

## Adjusting config

### Who do I talk to? ###

* Aaron (aaron@emagine.ie)
* David (david@emagine.ie)
* Mark (mark@emagine.ie)
* Sam (sam@emagine.ie)

### Approach
* Added Assets
* Added meta information and favicon
* Created Global Components - fonts, colour scheme  etc
* Creeated Elements - button, p, headings
* Created Card
* Created Header and Footer
* Generated template

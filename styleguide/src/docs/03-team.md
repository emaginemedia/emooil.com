---

title: Team

---

The website was built with love and care by the fine people at Emagine.

## Contact Information

Email:    waterford@emagine.ie

Waterford Office:
Address:   21 William Street, Waterford.
Phone:  051 36 40 40

Kildare Office:
Address:   Unit D1, M4 Business Park, Celbridge, Co. Kildare.
Phone:   01 685 3222


## Visit our website to see our favourite work, we're much more than just web

http://www.emagine.ie

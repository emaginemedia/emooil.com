---

title: Boilerplate

---

Build individual components that come together to form a final website/app. It may not be suitable for all projects, so mix and match to your needs.

Ideally, your source components will be used in the final site if it uses Twig for templates. 

This styleguide is a visual representation of the components that will be used within the "boilerplate" website. These should but may not necessarily always represent the final components used on the live site.
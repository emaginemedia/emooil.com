---

title: Coding Guide

---

There are a couple of tools pre-installed to help maintain consistent code.

It's recommended you find an ESLint and Stylelint plugin for your editor of choice. This will help find any potential issues as you code.

In the repository, tests will be run against these regardless and report back with success or fail. It's no biggie - these can be fixed easily, and in many cases will be fixed automatically for you.

## ESLint

Before any commit is made, ESLint will try and fix code if any issues are found. Before a commit is accepted, ESLint will run and report any errors which must be fixed before you can successfully commit.

## Stylelint

Same deal as ESLint above, except for your styles. Guidelines are still being worked out, but most issues can be automatically fixed.

## Atomic Design Principles

We always code with atomic design principles in mind, begin with the smallest possible atoms, then build molecules, then, organisms, or in our case, elements, objects, components. 

## Coding Best Practice

Code should be commented consistently, and we are striving for the highest level of efficiency, don't scope where it's not necessary, and build reusable and recyclable mixins. 
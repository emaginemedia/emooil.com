<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ExpressionEngine Config Items
// Find more configs and overrides at
// https://docs.expressionengine.com/latest/general/system_configuration_overrides.html

$config['app_version'] = '5.3.2';
$config['encryption_key'] = '5fbe8a97010a4843b12ffa4b7a6eeba74a06753d';
$config['session_crypt_key'] = '8659d124ed67f31638e30ad4669533ebac668a57';
$config['database'] = array(
	'expressionengine' => array(
		'hostname' => 'localhost',
		'database' => 'ee_boilerplate',
		'username' => 'root',
		'password' => 'lullabye',
		'dbprefix' => 'exp_',
		'char_set' => 'utf8mb4',
		'dbcollat' => 'utf8mb4_unicode_ci',
		'port'     => ''
	),
);

// EOF

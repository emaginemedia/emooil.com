# Boilerplate v3.0 - CMS

## Understanding the boilerplate v3.0 directory structure

This boilerplate is utilising what we hope will become standard for repo structure internally in the future.

The root folder of the repo, will contain 4 primary folders

* cms
	* Utilise the cms folder when using any CMS that allows you to place its core files above the webroot (i.e. Craft, Expression Engine)
	* You don't need to call this folder `cms`, its encouraged that you rename it in the case of EE, and for craft, just call it `craft`. Do try however, to keep the directory order the same, so if renaming, the folder to `Jeff` for example, try `_Jeff` instead.
	* In the case of a WordPress install, you should delete this folder, and place the core files within the `public_html` folder
* db
	* The db folder is used for maintaining DB backups, seperated by live, staging, and dev
* public_html
	* The public_html folder should contain all files that are intended to be live on the server root. If you're using MAMP, Laravel Valet or similar, your environment root should point here.
	* If you're using Wordpress, place all wordpress core files within this folder.

## Installing a CMS

*Craft:*

- Setup a localhost environment that meets the [minimum requirements](https://craftcms.com/docs/requirements)
- Point your localhost environment webroot to `public_html`
- Download the latest stable [Craft release](https://craftcms.com/)
- Place all core craft files in the `cms` folder, and rename the folder to `Craft`
- Place all public craft files in the `public_html` folder
- set permissions on the folders within the repo, to do this quickly, you can use `chmod -R 777 repo-name` within terminal from the level you keep all of your repos in.
- Create a database for Craft CMS to use. Use either `utf8` or `utf8_uncicode_ci`
- Navigate to /craft/config/db and fill out the database details
- Run the Craft installer by going to http://my-local-site.local/admin
- Backup your database after a successful install to the `db/local` folder

*Expression Engine:*

- Setup a localhost environment that meets the [minimum requirements](https://docs.expressionengine.com/v3/installation/requirements.html)
- Point your localhost environment webroot to `public_html`
- Obtain the latest stable EE release from Elvis/Production/Software/Expression Engine/builds
- Place all of the `system` folder files in the `cms` folder, and rename the folder to your name of choice
- Place all of the other EE file in the `public_html` folder
- set permissions on the folders within the repo, to do this quickly, you can use `chmod -R 777 repo-name` within terminal from the level you keep all of your repos in.
- Create a database for EE to use.
- Update your EE System path to reflect your core EE files being above the webroot, to do so, open both `index.php` and `admin.php` within the `public_html` folder, and update this line to reflect where your system folder files are now located: `$system_path = `./system`;`
- Run the EE installer by going to http://my-local-site.local/admin.php
- Backup your database after a successful install to the `db/local` folder

*Wordpress:*

- Setup a localhost environment that meets the [minimum requirements](https://wordpress.org/about/requirements/)
- Point your localhost environment webroot to `public_html`
- Download the latest stable [Wordpress release](https://wordpress.org/download/)
- Place all core Wordpress files in the `public_html` folder
- Create a database for Wordpress to use.
- Run the Wordpress installer by going to http://my-local-site.local/wp-admin
- At this stage, wordpress is installed, but you probably want to get up and running with our [starter theme](https://bitbucket.org/emaginemedia/emagine-wordpress-twig-starter-theme) which uses timber.
- Backup your database after a successful install to the `db/local` folder

### Who do I talk to? ###

* Aaron (aaron@emagine.ie)
* David (david@emagine.ie)
* Donnchadh (donnchadh@emagine.ie)
* Mark (mark@emagine.ie)
* Sam (sam@emagine.ie)